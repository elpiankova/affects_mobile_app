from django.conf.urls import patterns, include, url
from data_collector import views
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    # url(r'^$', include('data_collector.urls')),
    url(r'^$', views.start_collector_response),
    url(r'^service/$', views.data_collector_response),
    # url(r'^test_page/$', views.test_view),

    url(r'^admin/', include(admin.site.urls)),
)
