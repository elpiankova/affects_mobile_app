import dateutil.parser
from datetime import datetime
from data_collector.parsers.base_parser import BaseParser
import json
from value_container import ServiceValueDict
import math

class AEOvalParser(BaseParser):
    ''' '''

    def __call__(self, raw_data, parameter_list):
        self.refresh_values()
        
        lines = raw_data.splitlines()
    
        datetime_s = lines[6].split(':', 1)[1].strip()
        self.value['time'] = dateutil.parser.parse(datetime_s, dayfirst=True)
        self.utc_localize_time()
        
        self.map_dict = self.get_map_structure()
        self.map_dict["min_lat"] = 50.00
        self.map_dict["max_lat"] = 80.00
        self.map_dict["min_lon"] = -30.00
        self.map_dict["max_lon"] = 70.00
        
        data_lines = lines[10:]
        
        for i in range(len(data_lines)):
            [lat, lon] = data_lines[i].split()
            if i == 0 and lat == 'NaN' and lon == 'NaN':
                self.value['value'] = ' Oval is not determined owing to low \
geomagnetic activity or magnetic local time within Cusp or Harang discontinuity \
sectors' 
                break
            self.map_dict["layer"]["data"].append((float(lat), float(lon), 1))
        
        self.map_dict["layer"]['title'] = parameter_list[0].name
                
        self.map_dict["layer"]["scale"]["min"] = -127
        self.map_dict["layer"]["scale"]["max"] =  128
                  
        if not self.value['value']:
            self.value['value'] = json.dumps(self.map_dict)
        self.value['parameter'] = parameter_list[0]
            
        if not None in self.value.values():
            self.value_list.append(self.value)
        else:
            raise Exception("Some value isn't fully filled")
#         self.value_list.append(self.value)  
        return self.value_list

class AEOvalMagneticParser(BaseParser):
    ''' '''
    def __call__(self, raw_data, parameter_list):
        self.refresh_values()
        line = raw_data.splitlines()[-1]
        item = map(float, line.split())
        
        time= datetime(year=int(item[2]), month=int(item[1]), day=int(item[0]),
                       hour=int(item[3]), minute=int(item[4]), second=int(item[5]))
        
        for parameter in parameter_list:
            if 'poleward' in parameter.name:
                mag_lat = item[6]
            elif 'equatorward' in parameter.name:
                mag_lat = item[7]
            if math.isnan(mag_lat):
                data = 'Oval is not determined owing to low \
geomagnetic activity or magnetic local time within Cusp or Harang discontinuity \
sectors' 
            else:
                data = {"radius": 111.135*mag_lat,
                        "centre": [85.9, -147.]}
            self.value = ServiceValueDict(time=time,parameter=parameter,
                                          value=json.dumps(data))
            self.utc_localize_time()
            self.value_list.append(self.value)        
        return self.value_list

ae_oval_magnetic_parser = AEOvalMagneticParser()
ae_oval_parser = AEOvalParser()


""" TEST PART that performs when module launch as separate file"""
if __name__ == "__main__":
    print "Start tests"
    import requests
    from data_collector.models import Parameter
#     r = requests.get('http://fox.phys.uit.no/AFFECTS/RT_PW_location.dat')
#     output = ae_oval_parser(r.text, Parameter.objects.filter(name='Auroral electrojet poleward boundary'))
#     print output
#     try:
#         v = json.loads(output[0]['value'])
#     except ValueError:
#         v = output[0]['value']
#     print v
    r = requests.get('http://fox.phys.uit.no/AFFECTS/RT_oval_location.dat')
    output = ae_oval_magnetic_parser(r.text, Parameter.objects.filter(name__contains='magnetic'))
    print output

        
    