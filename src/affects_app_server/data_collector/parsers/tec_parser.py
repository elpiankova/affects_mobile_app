import dateutil.parser
from data_collector.parsers.base_parser import BaseParser
import json
import math

class TECParser(BaseParser):
    """
    Functor class for parser of AFFECT TEC real and forecast services output.
    """
    def __call__(self, raw_data, parameter_list):
        """
        @param raw_data: string raw data will be parsed
        @param parameter_list: list of instance of data_collector.model.Parameter
        class. Its value will be obtained after parsing
        """
        self.refresh_values()
        [header, data_s]= raw_data.split("\n", 1)
        
        header_list = header.split(',')
          
        self.value['time'] = dateutil.parser.parse(header_list[1].strip())
        self.utc_localize_time()
        
        self.map_dict = self.get_map_structure()
        
        self.map_dict["layer"]['title'] = parameter_list[0].name
        self.map_dict["layer"]["time"] = str(self.value['time'])
        
        self.map_dict["layer"]["scale"]["title"] = header_list[0].split(':')[0][:-5].replace('Assim',' Assim')
        self.map_dict["layer"]["scale"]["units"] = header_list[0].split(':')[1].strip()
        self.map_dict['min_lat'] = float(header_list[2].split('=')[1].strip())       
        self.map_dict['max_lat'] = float(header_list[3].split('=')[1].strip())       
        self.map_dict['min_lon'] = float(header_list[4].split('=')[1].strip())
        self.map_dict['max_lon'] = float(header_list[5].split('=')[1].strip())
        
        step_lat = float(header_list[6].split('=')[1].strip())
        step_lon = float(header_list[7].split('=')[1].strip())
        
        data_lines = data_s.splitlines()
        for i in range(len(data_lines)):
            value = data_lines[i].split()
            for j in range(len(value)):
                t = (self.map_dict['min_lat'] + i*step_lat,
                     self.map_dict['min_lon'] + j*step_lon,
                     float(value[j]))
                self.map_dict["layer"]["data"].append(t)
        
        try:
            self.map_dict["layer"]["scale"]["min"] = math.floor(min(
                    self.map_dict["layer"]["data"][i][2] for i in range(len(self.map_dict["layer"]["data"]))))
        except Exception, e:
            print "START DEBUG INFORMATION FOR TEC BUG"
            print "TEC exception: ", e
            import pickle
            F1 = open("TECdump_struct.txt", 'wb')
            pickle.dump(self.map_dict, F1)
            F2 = open('TECdump_rawdata.txt', 'wb')
            pickle.dump(data_lines, F2)
            
        self.map_dict["layer"]["scale"]["max"] = math.ceil(max(
                self.map_dict["layer"]["data"][i][2] for i in range(len(self.map_dict["layer"]["data"]))))
        
        self.value['value'] = json.dumps(self.map_dict)
        self.value['parameter'] = parameter_list[0]

        if not None in self.value.values():
            self.value_list.append(self.value)
        else:
            raise Exception("Some value isn't fully filled")
        
        return self.value_list
    
tec_parser = TECParser()

""" TEST PART that performs when module launch as separate file"""
if __name__ == "__main__":
    print "Start tests"
    import requests
    from data_collector.models import Parameter
    r = requests.get('http://swaciwebdevelop.dlr.de/fileadmin/PUBLIC/AFFECTS/TEC.dat')
    print tec_parser(r.text, Parameter.objects.filter(name='TEC real'))
    r = requests.get('http://swaciwebdevelop.dlr.de/fileadmin/PUBLIC/AFFECTS/TEC.dat')
    print tec_parser(r.text, Parameter.objects.filter(name='TEC real'))
    
    # print ursigram_parser(r.text, ['TEC real',])
