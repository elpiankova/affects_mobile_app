from data_collector.parsers.base_parser import BaseParser, ServiceValueDict
from datetime import datetime

class KpParser(BaseParser):
    ''' '''
    def __call__(self, raw_data, parameter_list):
        self.refresh_values()
        lines = raw_data.splitlines()
        for line in lines:
            try:
                item = map(int, line.split())
            except ValueError:
                break
#             print item
            if item:

                time = datetime(year=item[0], month=item[1], 
                                              day=item[2], hour=item[3])

                for parameter in parameter_list:
                             
                    if parameter.name == 'Kp real':
                        value = item[4]
                    elif parameter.name == 'Kp forecast':
                        value = item[5]
                        
                    if value != 99:
                        value /= 10.0
                        self.value = ServiceValueDict(time=time,parameter=parameter,
                                                      value=value)
                        self.utc_localize_time()
                        self.value_list.append(self.value)                
        
#         print [self.value_list[i]['value'] for i in range(len(self.value_list))]
        
        return self.value_list

    
kp_parser = KpParser()

if __name__=='__main__':
    f = open('/home/elena/affects_mobile_app/init_data/ver15_16_17/demon_preprocessor_processor16_2/demon_preprocessor_processor16_ubuntu/download2/Kp_latest.asc')
    raw_data = f.read()
    print raw_data
    from data_collector.models import Parameter
    parameters = Parameter.objects.filter(service__name = 'Kp forecast')
    print kp_parser(raw_data, parameters)
    
    
    