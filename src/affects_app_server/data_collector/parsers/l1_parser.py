from data_collector.parsers.base_parser import BaseParser, ServiceValueDict
import dateutil.parser, pytz, json


class L1DataParser(BaseParser):
    ''' '''
    def __call__(self, raw_data, parameter_list):
        '''parameter_list: [<Parameter: Activity level>, <Parameter: Ap estimate>, 
                            <Parameter: Auroral boundary>, <Parameter: Bz>, 
                            <Parameter: Kp estimate>, <Parameter: Solar wind velocity>]'''
        self.refresh_values()
        short_names = ['activity level', 'ap estimate', 'auroral position', 'Bz_min',
                       'Kp estimate', 'V_max']
        (names, units, values) = [line.split('\t') for line in raw_data.splitlines()[-3:]]
        time = pytz.utc.localize(dateutil.parser.parse(values[0]))

        for (short_name, parameter) in zip(short_names, parameter_list):
            index = names.index(short_name)
            try:
                self.value = ServiceValueDict(
                                    time=time, 
                                    parameter=parameter,
                                    value=json.dumps((float(values[index]), 
                                                      units[index].strip('[-]'))))
                self.value_list.append(self.value)
                
            except ValueError, e:
                print ValueError
                print "START debug info"
                print "RAW DATA:"
                print raw_data, '\n'
                print "Parameter:", parameter
                print "Value:", values[index]
                print "Time:", time
                print "END debug info"
#                 raise ValueError(e)
        
        return self.value_list
              
        
l1_parser = L1DataParser()

if __name__ == '__main__':
    print "Start tests"
    import requests
    from data_collector.models import Parameter
    r = requests.get('http://www.affects-fp7.eu/app-services/L1-Alerts/dataL1Alerts.txt')
    print l1_parser(r.text, Parameter.objects.filter(service__name='L1 Alerts'))