from data_collector.parsers.base_parser import BaseParser, ServiceValueDict
from datetime import datetime

class DstParser(BaseParser):
    ''' '''

    def __call__(self, raw_data, parameter_list):
        self.refresh_values()
        lines = raw_data.split('\n')
        for line in lines:
            try:
                item = map(int, line.split())
            except ValueError:
                break
#             print item
            if item:

                time = datetime(year=item[0], month=item[1], 
                                              day=item[2], hour=item[3])

                for parameter in parameter_list:             
                    
                    if parameter.name == 'Dst real':
                        value = item[4]
                    elif parameter.name == '1hr forecast':
                        value = item[5]
                    elif parameter.name == '2hr forecast':
                        value = item[6]
                    elif parameter.name == '3hr forecast':
                        value = item[7]
                    elif parameter.name == '4hr forecast':
                        value = item[8]
                    
#                     print self.value['parameter'], self.value['value']
                        
                    if value != 9999:
                        self.value = ServiceValueDict(time=time,parameter=parameter,
                                                      value=value)
                        self.utc_localize_time()
                        self.value_list.append(self.value)                
        
        return self.value_list
    
dst_parser = DstParser()

