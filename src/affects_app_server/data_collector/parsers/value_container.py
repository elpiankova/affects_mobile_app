from UserDict import UserDict
from UserList import UserList
from datetime import datetime
from data_collector.models import Parameter

class ServiceValueDict(UserDict):
    ''' 
    Dictionary-like-class that contains 'time', 'value', 'parameter' keys 
    only. It corresponds Value database model.
    Item of 'time' key must be 'datetime.datetime' type.
    Item of 'parameter' key must be instance of 'data_collector.models.Parameter'
    class.
    '''
    def __init__(self, time=None, value=None, parameter = None):
        UserDict.__init__(self)
        self['time'] = time
        self['value'] = value
        self['parameter'] = parameter
        
    def __setitem__(self, key, item):
        if key in ['time', 'value', 'parameter']:
            if key == 'time' and item is not None:
                if isinstance(item, datetime):
                    self.data[key] = item
                else:
                    raise TypeError("ServiceValueDict instance must have time of 'datetime.datetime' type")
            elif key == 'parameter' and item is not None:
                if isinstance(item, Parameter):
                    self.data[key] = item
                else:
                    raise TypeError("ServiceValueDict instance must have parameter of 'data_collector.models.Parameter' class")
            else:
                self.data[key] = item
        else:
            raise Exception("ServiceValueDict can't contain key %s" % str(key))
        
    def update(self, dict_=None, **kwargs):
        if dict_ is None:
            pass
        else:
            for k, v in dict_.items():
                self[k] = v
        if len(kwargs):
            if isinstance(kwargs, ServiceValueDict):
                self.data.update(kwargs)
                
 
class ServiceValueList(UserList): 
    ''' List-like-class contains only dictionary for Value database model 
        with keys 'time', 'value', 'parameter' only.
        For example:
        [{'time': __, 'value': __, 'parameter': __}, 
         {'time': __, 'value': __, 'parameter': __},
         ...]
    '''
    def __init__(self, initlist=None):
        UserList.__init__(self)
        if initlist is not None:
            if isinstance(initlist, ServiceValueList):
                self.data[:] = initlist.data[:]
                
    def __setitem__(self, i, item):
        if isinstance(item, ServiceValueDict):
            self.data[i] = item
        else:
            raise Exception("ServiceValueList can contain only ServiceValueDict")
        
    def __setslice__(self, i, j, other):
        i = max(i, 0); j = max(j, 0)
        if isinstance(other, ServiceValueList):
            self.data[i:j] = other.data
            
    def __add__(self, other):
        if isinstance(other, ServiceValueList):
            return self.__class__(self.data + other.data)
    
    def append(self, item):
        if isinstance(item, ServiceValueDict):
            self.data.append(item)
        else:
            raise Exception("ServiceValueList can contain only ServiceValueDict")
    
    def insert(self, i, item):
        if isinstance(item, ServiceValueDict):
            self.data.insert(i, item)     
        else:
            raise Exception("ServiceValueList can contain only ServiceValueDict")

    def extend(self, other):
        if isinstance(other, ServiceValueList):
            self.data.extend(other.data)
