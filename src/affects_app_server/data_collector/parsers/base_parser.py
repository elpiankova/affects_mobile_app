from value_container import ServiceValueDict, ServiceValueList
import pytz

class BaseParser(object):
    ''' Abstract-like functor for defining parsers interface.
        This class will be base for all parsers    
    '''
    def __init__(self):
        self.value_list = ServiceValueList()
        self.value = ServiceValueDict()
    def __call__(self, raw_data=None, parameter_list=None):        
        return self.value_list
    
    def utc_localize_time(self):
        self.value['time'] = pytz.utc.localize(self.value['time'])
        
    def refresh_values(self):
        self.value_list = ServiceValueList()
        self.value = ServiceValueDict()
        
    def get_map_structure(self):
        return {"min_lat": None,
                "max_lat": None,
                "min_lon": None,
                "max_lon": None,
                "layer": {"data":  [],
                          "scale": {"title": None,
                                    "units": None,
                                    "min"  : None,
                                    "max"  : None }
                          }
                }

base_parser = BaseParser()
