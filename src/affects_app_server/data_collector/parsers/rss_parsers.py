import dateutil.parser
from data_collector.parsers.base_parser import BaseParser
from value_container import ServiceValueDict
import xmltodict
import copy
import json

class UrsigramRSSParser(BaseParser):
    def __call__(self, raw_data, parameter_list):
        self.refresh_values()
        rss_data = xmltodict.parse(raw_data)
        
        last_entry = rss_data['messages']['channel']['item'][0]

        for entry in (last_entry,):
            self.value = ServiceValueDict(parameter=parameter_list[0])
            if entry['noticeable_events']:
                noticable_events = entry['noticeable_events']['event']
                if isinstance(noticable_events, dict):
                    noticable_events = [copy.deepcopy(noticable_events)]
                for event in noticable_events:
                    if event['loc']['latitude'] == None: 
                        event['location'] = '-'
                        continue
                    lat, lon = float(event['loc']['latitude']), float(event['loc']['longitude'])
                    if lat >= 0:
                        event['location'] = 'N%02.f' % lat
                    else:
                        event['location'] = 'S%02.f' % abs(lat)
                    if lon >= 0:
                        event['location'] += 'W%02.f' % lon
                    else:
                        event['location'] += 'E%02.f' % abs(lon)
                entry['noticeable_events']['event'] =noticable_events         
            context = {'service_name': self.value['parameter'].service.name, 
                       'entry': entry,
                       'flux_and_ap_prediction': 
                        zip(entry['flux_10cm_prediction']['prediction'],
                            entry['ap_prediction']['prediction'])}

            self.value['value'] = json.dumps(context)
            self.value['time'] = dateutil.parser.parse(entry['pubDate'])
        
            if not None in self.value.values():
                self.value_list.append(self.value)
            else:
                raise Exception("Some value isn't fully filled")
        
        return self.value_list


class PrestoRSSParser(BaseParser):
    def __call__(self, raw_data, parameter_list):
        self.refresh_values()
        rss_data = xmltodict.parse(raw_data)
#         template = loader.get_template('presto_template.html')
        
        last_entry = rss_data['rss']['channel']['item'][0]
        
        for entry in (last_entry,):
            self.value = ServiceValueDict(parameter=parameter_list[0])
            
            context = {'service_name': self.value['parameter'].service.name,
                       'entry': entry}
            self.value['value'] = json.dumps(context)
            self.value['time'] = dateutil.parser.parse(entry['pubDate'])
        
            if not None in self.value.values():
                self.value_list.append(self.value)
            else:
                raise Exception("Some value isn't fully filled")
        
        return self.value_list


ursigram_rss_parser = UrsigramRSSParser()

presto_rss_parser = PrestoRSSParser()

if __name__ == "__main__":

    from data_collector.models import Parameter
    import requests
    raw_data = requests.get('http://sidc.be/archive/prestorss').text
    print presto_rss_parser(raw_data, [Parameter.objects.get(name__contains='presto')])[0]['value']
#     raw_data = requests.get('http://sidc.be/archive/ursigramrss').text
#     print ursigram_rss_parser(raw_data, 
#                         [Parameter.objects.get(name__contains='Ursigram')])[0]['value']
#     f = open('/home/elena/affects_mobile_app/src/affects_app_server/data_collector/templates/temp.html', 'w')
#     f.write(ursigram_rss_parser(raw_data, [Parameter.objects.get(name__contains='Ursigram')])[0]['value'])
    