from threading import Timer
import datetime, pytz, os
from data_collector.downloader import Downloader
from data_collector.models import Value, Parameter
from data_collector.parsers import *

class CollectorLoop(object):
    def __init__(self, function, source=None):
        self.source = Downloader(source)
        self.started = False
        self._timer = None
        self._function = function

    def start(self, period=1):
        """
        Start to collect
        @param period: interval in seconds will be executed
        """
        if not self.started:
            self._timer = Timer(period, self._loop, (period,))
            self._timer.start()
            self.started = True

    def stop(self):
        """
        Stop collect
        """
        if self.started:
            self._timer.cancel()
            self.started = False

    def _loop(self, period):
        self.started = False
        print datetime.datetime.now()
        print "Do something with %s" % self.source
        self._function(self.source)
        self.start(period)
        pass

home_dir = os.getenv("HOME")

def add_data_to_db(url_downloader):
    success = False
    response = url_downloader.get()
    if response == None:
        print "Download Error: %s with URL: %s" % (url_downloader.error,url_downloader.source_url)
        try:
            frw = open(os.path.join(home_dir, url_downloader.source_url.url))
            raw_data = frw.read()
        except Exception, e:
            print "File exception: ", e
            return
    else:
        raw_data = response.text
    
    parser = eval(url_downloader.source_url.parser_type)
    parameter_set = url_downloader.source_url.parameter_set.all()
    
    if parameter_set:
        value_list = parser(raw_data, parameter_set)
        for v in value_list:
            try:
                Value.objects.create(**v)
                success = True
            except Exception, e:
                print 'No join. Exception: %s' % e
                
    if success:
        service = parameter_set.first().service
        service.last_update_time = pytz.utc.localize(datetime.datetime.now())
        service.save()
        print "Last update time %s - %s" % (service, service.last_update_time)

def clean_old_data(*args):
    print "Deleting data"
    for parameter in Parameter.objects.all():
        if parameter.value_set.last():
            last_value_time = parameter.value_set.last().time
            for value in parameter.value_set.filter(time__lt=last_value_time-datetime.timedelta(1)):
                value.delete()

def collector_loop():
#     import sys
#     sys.stdout = open('script_log.txt', 'w')
    from data_collector.models import SourceUrl
#     source = SourceUrl.objects.get(url="http://swaciwebdevelop.dlr.de/fileadmin/PUBLIC/AFFECTS/TEC.dat")
#     collector = CollectorLoop(source)

    deletor = CollectorLoop(clean_old_data)
    deletor.start(21600)
    clean_old_data()
    
    print "Start to collect"
    for source in SourceUrl.objects.all():
        print source
        collector = CollectorLoop(add_data_to_db, source)
        add_data_to_db(collector.source)
        try:
            update_period = source.parameter_set.first().service.update_period
        except:
            pass
        finally:
            if update_period:
                print("Source: %s, period: %s" % (source, update_period))
                collector.start(update_period) 
    
    
# collector_loop()