"Different utilities and tools for work with class"

class AttrDisplay:
    """
    Implements the inherited method of output operation overload 
    that displays the names of classes and all attributes of instances
    in the form of pairs: name=value
    """
    def _gatherAttrs(self):
        attrs = []
        for key in sorted(self.__dict__):
            attrs.append('%s=%s' % (key, getattr(self, key)))
        return ', '.join(attrs)
    
    def __str__(self):
        return '[%s: %s]' % (self.__class__.__name__, self._gatherAttrs())

    
""" TEST PART (performs when module launch as separate file)"""
if __name__ == '__main__':
    class TopTest(AttrDisplay):
        count = 0
        def __init__(self):
            self.attr1 = TopTest.count
            self.attr2 = TopTest.count + 1
            TopTest.count += 2
    
    class SubTest(TopTest):
        pass
    
    X, Y = TopTest(), SubTest()
    
    print X
    print Y
    