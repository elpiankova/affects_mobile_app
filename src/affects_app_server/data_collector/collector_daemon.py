#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.abspath('..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "affects_app_server.settings")

from daemon import runner
from data_collector.collector_loop import collector_loop

import datetime
time_start = datetime.datetime.now()
s_time_start = str(time_start).replace(' ', 'T').split('.')[0]

class App():
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = 'script_log%s.txt' % s_time_start
        self.stderr_path = '/dev/tty'
        self.pidfile_path =  '/tmp/foo.pid'
        self.pidfile_timeout = 5
    def run(self):
        print "Start collector loop"
        collector_loop()


app = App()

# class MyDaemonRummer(runner.DaemonRunner):
#     def _restart(self):
#         try:
#             runner.DaemonRunner._restart(self)
#         finally:
#             self._start()
    

daemon_runner = runner.DaemonRunner(app)
daemon_runner.do_action()


# from data_collector.models import SourceUrl, Value
# from data_collector.parsers import ae_oval_parser, dst_parser, kp_parser, ae_oval_magnetic_parser
# from data_collector.parsers import tec_parser, ursigram_parser
# from data_collector.parsers.rss_parsers import presto_rss_parser, ursigram_rss_parser
# from data_collector.downloader import Downloader
# import requests
# import datetime, pytz
# 
# 
# ''' DEBUG '''
# # Value.objects.filter(parameter__name__contains = 'Kp').delete()
# # Value.objects.filter(parameter__name__contains = 'SIDC Ursigram').last().delete()
# ''' DEBUG '''
# 
# # for source in SourceUrl.objects.filter(title__contains = 'Auroral'): 
# for source in SourceUrl.objects.all():
#     raw_data = ''
#     success = False
#     try:
# #         raw_data = Downloader(source.url).get()
#         print "Url: ", source.url
#          
#         raw_data = requests.get(source.url)
#         raw_data = raw_data.text
#     except:
#         try:
#             f = open(os.path.join(home_dir, source.url))
#             raw_data = f.read()
#         except: continue
#     
#     print "Parser type: ", source.parser_type
#     parser = eval(source.parser_type)
#     print source.parameter_set.all()
#     if source.parameter_set.all() and raw_data:
#         value_list = parser(raw_data, source.parameter_set.all())
#         for v in value_list:
#             print v
#             try:
#                 Value.objects.create(**v)
#                 success = True
#             except:
#                 print('No join')
#                 pass
#     if success:        
#         service = source.parameter_set.first().service
#         service.last_update_time = pytz.utc.localize(datetime.datetime.now())
#         service.save()
#         print service, service.last_update_time

            



# from data_collector.collector_loop import Collector
# print "Start to collect"
# for source in SourceUrl.objects.all():
#     print source
#     collector = Collector(source)
#     try:
#         update_period = source.parameter_set.first().service.update_period
#     except:
#         pass
#     finally:
#         if update_period:
#             print("Source: %s, period: %s" % (source, update_period))
#             collector.start(update_period)


