import json
import datetime
# from django.utils.timezone import utc
from data_collector.models import Service, Value
import webcolors


def json_services_info():
    resp = []
    for service in Service.objects.all():
        service_dic = {}
        for field in Service._meta.fields:
            if field.attname == 'name':
                service_dic['service'] = service.serializable_value(field.attname)
            elif isinstance(service.serializable_value(field.attname),
                            datetime.datetime):
                service_dic[field.attname] = str(service.serializable_value(field.attname))
            else:
                service_dic[field.attname] = service.serializable_value(field.attname)
            
#             if isinstance(service_dic[field.attname], datetime.datetime):
#                 service_dic[field.attname] = str(service.serializable_value(field.attname))
        resp.append(service_dic)
    return json.dumps(resp)


def build_map_data(service):
    result = {'title': service.name}
    value_dict = []
    
    if service.name in ['TEC Europe']:
        parameter_set = service.parameter_set.all()
    elif service.name in ['Realtime oval']:
        parameter_set = service.parameter_set.filter(name__contains='boundary')
    
    for parameter in parameter_set:        
        last_value = parameter.value_set.last()
        if last_value is None: return
        try:
            v = json.loads(last_value.value)
        except ValueError:
            result['type'] = 'error'
            result['message'] = last_value.value
            result['time'] = str(last_value.time)
            return result
        v['time'] = last_value.time
        value_dict.append(v.copy())
#     print "Value dict: ", value_dict
    
    if service.name == 'Realtime oval':
        parameter = service.parameter_set.get(name__contains='magnetic latitude (equatorward)')
        last_value = parameter.value_set.last()
        for value_dict_item in value_dict:
            if 'equatorward' in value_dict_item['layer']['title']:
                value_dict_item['layer']['circle'] = json.loads(last_value.value)
        parameter = service.parameter_set.get(name__contains='magnetic latitude (poleward)')
        last_value = parameter.value_set.last()
        for value_dict_item in value_dict:
            if 'poleward' in value_dict_item['layer']['title']:
                value_dict_item['layer']['circle'] = json.loads(last_value.value)
    
    result['min_lat'] = min([value['min_lat'] for value in value_dict])    
    result['max_lat'] = max([value['max_lat'] for value in value_dict])    
    result['min_lon'] = min([value['min_lon'] for value in value_dict])    
    result['max_lon'] = max([value['max_lon'] for value in value_dict])    

    result['time'] = str(value_dict[-1]['time'])   
    result['layers'] = [value['layer'] for value in value_dict if isinstance(value, dict)]   
    if service.name in ['TEC Europe']:
        result['type'] = 'shading'
    elif service.name in ['Realtime oval']:
        result['type'] = 'line'
    return result


def build_plot_data(service):
    result = {'title': service.name,
              'time': 'Not used now',
              'X_min': "Not implemented now",
              'X_max': "Not implemented now",
              'X_step': 'Not used now',
              'Y_step': 'Not used now',
              'X_title': "UTC",
              'Y_title': service.name.split()[0],
              'curves': []}

    # Preparation
#     time_now = datetime.datetime.utcnow().replace(tzinfo=utc)
    colors = 'red', 'royalblue', 'lime', 'orange', 'yellow'
    if service.name == "Kp forecast":
        style = 'bar'
    else:
        style = 'line'
    
    value_all = Value.objects.filter(parameter__service = service)
    if not value_all: return
    
    time_last = value_all.last().time
    time_first = time_last - datetime.timedelta(hours=28)
    
    value_min = []; value_max = []; time_max = []
    for i, parameter in enumerate(service.parameter_set.all()):
        v = Value.objects.filter(parameter=parameter, time__gte=time_first)
        time_set = v.values_list('time', flat=True)
                
        data = (map(str, time_set), 
                map(float, v.values_list('value', flat=True)))
        
        value_min.append(min(data[1]))
        value_max.append(max(data[1]))
        if i == 0:
            result['X_min'] = str(time_set[0])
        time_max.append(time_set[len(time_set)-1])

        assert isinstance(parameter.name, unicode)
        curve = {'data': data, 
                 'color': (127,) + webcolors.name_to_rgb(colors[i]), 
                 'style': style,
                 'title': parameter.name}
        result['curves'].append(curve)
    
    result['Y_min'] = min(value_min)
    result['Y_max'] = max(value_max)
    result['X_max'] = str(max(time_max))
    
    return result


def build_text_data(service):
    from django.template import Context, loader
    
    last_value = Value.objects.filter(parameter__service=service).last()
    if last_value is None:
        return
    prefix = service.name.split()[1].lower() if 'SIDC' in service.name else '' 
    template = loader.get_template(prefix + '_template.html')
    context = Context(json.loads(last_value.value))
    html_page = template.render(context).replace('&lt;', '<').replace('&gt;', '>').replace('\n','')
    
    result = {"title": service.name,
              "time": str(last_value.time),
              "text": html_page}
    return result


def build_notification(service):
    result = {'title': service.name}
    
    if service.name in ['SIDC Presto']:
        result['type'] = 'simple_notification'
        last_value = Value.objects.filter(parameter__service=service).last()
        if last_value is None:
            return
        v = json.loads(last_value.value)
        result['text'] = v['entry']['description'].replace('<br />', '\n')
    elif service.name in ['L1 Alerts']:
        result['type'] = 'notification_with_conditions'
        result['parameters'] = []
        for parameter in service.parameter_set.all():        
            last_value = parameter.value_set.last()
            if last_value is None:
                return
            v = json.loads(last_value.value)
            parameter_view = {parameter.name: {'value': v[0],
                                               'units': v[1]}}
            result['parameters'].append(parameter_view)
    
    result['time'] = str(last_value.time)
        
    return result


def build_data(service, ui_type):
    return {"map":  build_map_data,
            "plot": build_plot_data,
            "report": build_text_data,
            "notification": build_notification}[ui_type.type](service)
    

def give_services_info(service_name_list, error = []):
    result = []
    service_list = Service.objects.filter(name__in=service_name_list)
    for service in service_list:
        service_json_responce = {'service': service.name}
        for ui_type in service.ui_types.all():
            data = build_data(service, ui_type)
            if data: service_json_responce[ui_type.type] = data
        result.append(service_json_responce)
    return json.dumps(result)
        
if __name__ == "__main__":
    service = Service.objects.get(name='L1 Alerts')
    print give_services_info([service])        
