from django.db import models

class Service(models.Model):
    ''' Class representing an AFFECTS services 
    This is the class storing the service name, service visualization type,
    last update time, update frequency
    Service name must be unique
    ...    
    '''
#     VISUALIZATION_TYPE = (
#         ('plot', 'plot'),
#         ('map', 'map'),
#         ('alert', 'alert'),
#         ('report', 'report'),
#         ('notification', 'notification'),
#     )
    
    name = models.CharField(max_length=255, primary_key=True)
#     visualization_type = models.CharField(max_length=12,
#                                           choices=VISUALIZATION_TYPE)
    ui_types = models.ManyToManyField('UiType', db_table='services_types')
    last_update_time = models.DateTimeField(null=True)
    update_period = models.IntegerField()   # value is stored in seconds

    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = 'services'
        
class UiType(models.Model):
    type = models.CharField(max_length=255, primary_key=True)
    
    def __unicode__(self):
        return self.type
    class Meta:
        db_table = 'ui_type'

class SourceUrl(models.Model):

    PARSER_TYPE = (
#         ('ursigram_parser', 'Ursigram'),
        ('tec_parser', 'TEC'),
        ('ae_oval_parser', 'AE oval'),
        ('ae_oval_magnetic_parser', 'AE oval magnetic'),
        ('dst_parser', 'Dst',),
        ('kp_parser', 'Kp'),
        ('presto_rss_parser', 'Presto RSS'),
        ('ursigram_rss_parser', 'Ursigram RSS'),
        ('l1_parser', 'L1 alerts'),
    )
    
    name = models.CharField(max_length=255, db_column='title')
    url = models.CharField(max_length=255, unique = True)
    parser_type = models.CharField(max_length=32,
                                   choices=PARSER_TYPE)
    def __unicode__(self):
        return self.url  

    class Meta:
        db_table = 'source_urls'    


class Parameter(models.Model): 
    
    name = models.CharField(max_length=255, primary_key=True)
    url = models.ForeignKey('SourceUrl', db_column='url_id',
                            on_delete=models.PROTECT)
    service = models.ForeignKey('Service', db_column='service_name',
                                on_delete=models.PROTECT) 
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = 'parameters'
        ordering = ['name']
        
class Value(models.Model):
 
    time = models.DateTimeField()
    value = models.TextField()  # or BinaryField()
    parameter = models.ForeignKey('Parameter', db_column='parameter_name')
    
    def __unicode__(self):
        return unicode(self.value)
    
    class Meta:
        db_table = 'values'
        unique_together = ("time", "parameter")
        ordering = ['time']
        get_latest_by = "time"
        
