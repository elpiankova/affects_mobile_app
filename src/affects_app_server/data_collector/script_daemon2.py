import os
import sys

home_dir = os.getenv("HOME")

sys.path.append(os.path.join(home_dir, 
                "affects_mobile_app/src/affects_app_server/"))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "affects_app_server.settings")

from data_collector.models import SourceUrl, Value
from data_collector.parsers import *
from data_collector.downloader import Downloader
# import requests
import datetime, pytz
import time

''' DEBUG '''
# Value.objects.filter(parameter__name__contains = 'Kp').delete()
# Value.objects.filter(parameter__name__contains = 'SIDC').delete()
''' DEBUG '''
f = open('script_log.txt', 'w')

while True:
    print 'Start downloading'
#     for source in SourceUrl.objects.filter(title__contains = 'Auroral'): 
    for source in SourceUrl.objects.all():
        raw_data = ''
        success = False
        
        downloader = Downloader(source)
        print "Url: "+ source.url
             
        response = downloader.get()
        print "Response: ", response
        if response == None:
            print "Download Error:", downloader.error
            try:
                frw = open(os.path.join(home_dir, source.url))
                raw_data = frw.read()
            except Exception, e:
                print "File exception: ", e
                continue
        else:
            raw_data = response.text

        print 'Downloading (or file opening) success'
        
#         print "Parser type: ", source.parser_type
        parser = eval(source.parser_type)
#         print source.parameter_set.all()
        if source.parameter_set.all() and raw_data:
            value_list = parser(raw_data, source.parameter_set.all())
            for v in value_list:
                f.write(str(v)+'\n')
                try:
                    Value.objects.create(**v)
                    success = True
                    print 'Join'
                except:
                    print 'No join'
                    pass

        if success:        
            service = source.parameter_set.first().service
            service.last_update_time = pytz.utc.localize(datetime.datetime.now())
            service.save()
            print service, service.last_update_time
    print "Start sleeping\n"
    time.sleep(120)
            



# from data_collector.collector_loop import Collector
# print "Start to collect"
# for source in SourceUrl.objects.all():
#     print source
#     collector = Collector(source)
#     try:
#         update_period = source.parameter_set.first().service.update_period
#     except:
#         pass
#     finally:
#         if update_period:
#             print("Source: %s, period: %s" % (source, update_period))
#             collector.start(update_period)


