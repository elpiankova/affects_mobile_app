import requests
from data_collector.common.classtools import AttrDisplay 
import dateutil.parser # ??????????????

class Downloader(AttrDisplay):
    def __init__(self, source_url=None):
        self.source_url = source_url
        self.response = None
        self.last_modified = None
        self.error = None
    
    def _rude_get(self):
        try:
            self.response = requests.get(self.source_url.url, timeout = 3)
        except requests.Timeout, t:
            self.error = t   # "Host is unreachable"
        except Exception, e: # yes, I know that's a bad way to do it...
            self.error = e
        else:
            self.last_modified = self.response.headers.get('last-modified')
            self.error = None  
            
    def get(self):
        try:
            r = requests.head(self.source_url.url)
        except Exception, e:
            self.error = e
            return self.response
        if 'last-modified' in r.headers.keys():
            if self.last_modified == r.headers.get('last-modified'):
                return self.response
        self._rude_get()
#         print "Errors: %s" % self.error
        return self.response
#                 

""" TEST PART that performs when module launch as separate file"""
if __name__ == "__main__":
    print "Start tests"
    from data_collector.models import SourceUrl
    url_s = SourceUrl.objects.get(url='http://fox.phys.uit.no/AFFECTS/RT_EW_location.dat')
#     url_s = 'http://fox.phys.uit.no/AFFECTS/RT_EW_location.dat'
#     url_s = 'http://www.sidc.be/archive/product/meu/latest'
    d = Downloader(url_s)
    print d
    print d.get().text.splitlines()[6]
    print d
    print d.get().text.splitlines()[6]
    print d 
    import time
    print "I'm sleeping now"
    time.sleep(130)
    print 'I wake up'
    print d.get().text.splitlines()[6]
    print d