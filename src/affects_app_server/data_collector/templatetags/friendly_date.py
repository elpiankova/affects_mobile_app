from django import template
from django.template.defaultfilters import stringfilter
import dateutil.parser

register = template.Library()

@register.filter
@stringfilter
def frindly_date(date):
    return dateutil.parser.parse(date).strftime('%d %b %Y')

@register.filter
@stringfilter
def frindly_datetime(date):
    return dateutil.parser.parse(date).strftime('%H:%MUT, %d %b %Y')