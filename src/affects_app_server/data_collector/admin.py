from django.contrib import admin
from data_collector.models import Service, Parameter, SourceUrl, UiType

admin.site.register(Service)

admin.site.register(Parameter)

admin.site.register(SourceUrl)

# admin.site.register(UiType)