#!/usr/bin/env python
#importing modules
#----------------------------------------------------------------------------------------------------------------------------------
import xml.etree.ElementTree as ET
from xml.dom import minidom
import datetime, sys, getopt, time, re, urllib2, os, socket, shutil
from string import *
from time import gmtime, strftime
from math import *
#----------------------------------------------------------------------------------------------------------------------------------
#Error xml file structure forming
defaultFileNames = ['AceMin.dat','AllData.dat','fraclaDimcal.asc','OutputPrediction','ErrorFile.xml']
defaultDirs = ['download/','download/','download/','download/','download/']
defaultorderfile = 'JobOrderG.xml'
generated_on = str(datetime.datetime.now())
root = ET.Element('Errorfile')
root.append(ET.Comment('Error File Generated'))
head = ET.SubElement(root, 'head')
title = ET.SubElement(head, 'title')
title.text = 'Errors of preprocessing'
dc = ET.SubElement(head, 'dateCreated')
dc.text = generated_on
body = ET.SubElement(root, 'ErrorsValuesInputFile')
body2 = ET.SubElement(root, 'ErrorsValuesFileDownload')
current_group1 = ET.SubElement(body, 'InputFileDir')
current_group2 = ET.SubElement(body, 'FileRead')
#Set values in the xml nodes
def importValues(current_group, filename, mark):
    podcast = ET.SubElement(current_group, filename)
    podcast.text = mark
    return (current_group, filename, mark)
#error prediction statistic file forming
TestFileroot = ET.Element('MetaFile')
ET.SubElement(TestFileroot, 'productname').text = 'Geomagnetic forecast module'
ET.SubElement(TestFileroot, 'stationname').text = 'diverse global'
ET.SubElement(TestFileroot, 'project').text = 'AFFECTS'
ET.SubElement(TestFileroot, 'product_creation_time').text = generated_on
ET.SubElement(TestFileroot, 'location').text = 'global'
ET.SubElement(TestFileroot, 'format').text = 'ASCII'
ET.SubElement(TestFileroot, 'data_type').text = 'nc'
ET.SubElement(TestFileroot, 'description').text = 'This forecast is issued without any warranties - use at your own risk'
ET.SubElement(TestFileroot, 'contact_person').text = 'Aleksei Parnowski parnowski@ikd.kiev.ua'
ET.SubElement(TestFileroot, 'contact_address').text = 'Space Research Institute, Prospekt Akademika Glushkova 40 building 4/1, 03680 MSP Kyiv-187, Ukraine'
ET.SubElement(TestFileroot, 'Processing_software_version').text = '2.0'
#----------------------------------------------------------------------------------------------------------------------------------
#Functions of files read and write
#Data write
def DataToFile(data, filename):
    File = open(filename, "wb")
    for i in range(0,len(data)):
        for x in data[i]:
            File.write(repr(x).rjust(12))
        File.write('\n')
    File.close()
#xml file write
def XmlToFileWr(root, outfilename):
    dom = minidom.parseString(ET.tostring(root)).toprettyxml()
    text_file = open(outfilename, "w")
    text_file.write(dom)
    text_file.close()
    return dom
#Input xml file write
def xmlread(filenamein):
	file = open(filenamein)
	data = file.read()
	file.close()
	dom = minidom.parseString(data)
	itemlist = dom.getElementsByTagName('Name')
	i = 0
	filename = range(0,5)
	for s in itemlist:
			nameTag = dom.getElementsByTagName('Name')[i].toxml()
			hobbyTag = dom.getElementsByTagName('Value')[i].toxml()
			name=nameTag.replace('<Name>','').replace('</Name>','')
			if name == 'filenameAceMin':
				hobby=hobbyTag.replace('<Value>','').replace('</Value>','')
				filename[0]=hobby
				i = i + 1
			elif name == 'filenameAlldata':
				hobby=hobbyTag.replace('<Value>','').replace('</Value>','')
				filename[1]=hobby
				i = i + 1
			elif name == 'filenameFractalCal':
				hobby=hobbyTag.replace('<Value>','').replace('</Value>','')
				filename[2]=hobby
				i = i + 1
			elif name == 'filenameOutfile':
				hobby=hobbyTag.replace('<Value>','').replace('</Value>','')
				filename[3]=hobby
				i = i + 1
			elif name == 'ErrorFile':
				hobby=hobbyTag.replace('<Value>','').replace('</Value>','')
				filename[4]=hobby
				i = i + 1
			else:
				i = i + 1	
	indirlist = dom.getElementsByTagName('Input')
	j = 0
	dirs = range(0,5)
	for s in indirlist:
			hobbyTag = dom.getElementsByTagName('File_Name_in_Mag')[j].toxml()
			hobby=hobbyTag.replace('<File_Name_in_Mag>','').replace('</File_Name_in_Mag>','')
			dirs[0] = hobby
			hobbyTag1 = dom.getElementsByTagName('File_Name_in_Swepam')[j].toxml()
			hobby1 = hobbyTag1.replace('<File_Name_in_Swepam>','').replace('</File_Name_in_Swepam>','')
			dirs[1] = hobby1
			hobbyTag2 = dom.getElementsByTagName('File_Name_in_Loc')[j].toxml()
			hobby2 = hobbyTag2.replace('<File_Name_in_Loc>','').replace('</File_Name_in_Loc>','')
			dirs[2] = hobby2
			hobbyTag3 = dom.getElementsByTagName('File_Name_in_Indeces')[j].toxml()
			hobby3 = hobbyTag3.replace('<File_Name_in_Indeces>','').replace('</File_Name_in_Indeces>','')
			dirs[3] = hobby3
			j = j + 1			

	outdirlist = dom.getElementsByTagName('Output')
	j = 0
	for s in outdirlist:
			hobbyTag1 = dom.getElementsByTagName('File_Name_out')[j].toxml()
			hobby1=hobbyTag1.replace('<File_Name_out>','').replace('</File_Name_out>','')
			dirs[4] = hobby1
			j = j + 1
	return (filename, dirs)
#----------------------------------------------------------------------------------------------------------------------------------
#Data download and read functions
#Check of the download path and it creation
def pathcheck(dirs):
    path1 = strftime("%Y", gmtime()) + strftime("%m", gmtime())+'/'
    downloaddirs = os.path.dirname(dirs+path1)
    try:
        os.stat(downloaddirs)
    except:
        os.makedirs(downloaddirs)
        return downloaddirs+'/'
    else:
        return downloaddirs+'/'
def outpathcheck(dirs):
    downloaddirs = os.path.dirname(dirs)
    try:
        os.stat(downloaddirs)
    except:
        os.makedirs(downloaddirs)
        return downloaddirs+'/'
    else:
        return downloaddirs+'/'
#File download to file
def FileDownload(remotefile,filename):
    localfile = open(filename,'wb')
    localfile.write(remotefile)
    localfile.close()
#Ace data download
def AceDownload(name, AceData, lines, pathdown):
    now_date = datetime.datetime.utcnow()
    delta = datetime.timedelta(days=29)
    delta1 = datetime.timedelta(days=1)
    now_date1 = now_date - delta
    flag = 1
    while now_date1 != now_date + delta1:
        filename = str(now_date1.strftime("%Y")+now_date1.strftime("%m")+now_date1.strftime("%d")+name)
        errname1 =  'FileRead'
        letter = 'File'
        errname=errname1+filename
        current_group3 = ET.SubElement(body2,errname)
        try:
            os.stat(pathdown+filename)
        except:
            importValues(current_group3, letter+filename, '1')
        else:
            importValues(current_group3, letter+filename, '0')
            AceData = AceDataFromFile(AceData, pathdown+filename, lines)
        now_date1 = now_date1 + delta1
    return (AceData, flag)
#Data read from file into list
def AceDataFromFile(AceData, filename, lines):
    count = 0
    with open(filename, 'r') as ff:
        for line in ff.readlines():
            if count > lines:
                AceData.append (list (map (float, line.split())))
            else:
                count = count + 1
    AceData = AceDataFileEndCheck(AceData)
    return AceData
#AceData file end check
def AceDataFileEndCheck(AceData):
    if(AceData[len(AceData)-1][3] != '2359' and AceData[len(AceData)-1][2] != int(datetime.datetime.utcnow().strftime("%d"))):
        test = int(AceData[len(AceData)-1][3])
        Year = AceData[len(AceData)-1][0]
        Month = AceData[len(AceData)-1][1]
        Day = AceData[len(AceData)-1][2]
        JDay = AceData[len(AceData)-1][4]
        if len(AceData[len(AceData)-1]) == 13:
            while(test < 2359):
                if (test-int(test/100)*100) == 59:
                    test = (int(test/100)+1)*100
                else:
                    test = test + 1
                AceData.append([Year, Month, Day, test, JDay, int(test)*60, 9,-999.9,-999.9,-999.9,-999.9,-999.9,-999.9])
        elif len(AceData[len(AceData)-1]) == 10:
            while(test < 2359):
                if (test-int(test/100)*100) == 59:
                    test = (int(test/100)+1)*100
                else:
                    test = test + 1
                AceData.append([Year, Month, Day, test, JDay, int(test)*60, 9,-9999.9,-9999.9,-1.00e+05])
        return AceData
    elif(AceData[len(AceData)-1][3] != '2359' and AceData[len(AceData)-1][2] == int(datetime.datetime.utcnow().strftime("%d"))):
        if(int(datetime.datetime.utcnow().strftime("%H"))-1 != int(int(AceData[len(AceData)-1][3])/100)):
            test = int(AceData[len(AceData)-1][3])
            Year = AceData[len(AceData)-1][0]
            Month = AceData[len(AceData)-1][1]
            Day = AceData[len(AceData)-1][2]
            JDay = AceData[len(AceData)-1][4]
            if len(AceData[len(AceData)-1]) == 13:
                while(test < ((int(datetime.datetime.utcnow().strftime("%H"))-1)*100+59)):
                    if (test-int(test/100)*100) == 59:
                        test = (int(test/100)+1)*100
                    else:
                        test = test + 1
                    AceData.append([Year, Month, Day, test, JDay, int(test)*60, 9,-999.9,-999.9,-999.9,-999.9,-999.9,-999.9])
            elif len(AceData[len(AceData)-1]) == 10:
                while(test < ((int(datetime.datetime.utcnow().strftime("%H"))-1)*100+59)):
                    if (test-int(test/100)*100) == 59:
                        test = (int(test/100)+1)*100
                    else:
                        test = test + 1
                    AceData.append([Year, Month, Day, test, JDay, int(test)*60, 9,-9999.9,-9999.9,-1.00e+05])
        return AceData
    else:
        return AceData
#Dst file download
def DstFileDownload(DstData, pathdown):
    timeout_v = 10
    socket.setdefaulttimeout(timeout_v)
    try:
        urlpath =urllib2.urlopen('http://wdc.kugi.kyoto-u.ac.jp/dst_realtime/', None, timeout = timeout_v)
    except urllib2.URLError, e:
        current_group3 = ET.SubElement(body2,'InetnetConnection')
        importValues(current_group3, 'wdc.kugi.kyoto-u.ac.jp', '1')
        DstControlParam = 1
    except socket.timeout, e:
        current_group3 = ET.SubElement(body2,'InetnetConnection')
        importValues(current_group3, 'wdc.kugi.kyoto-u.ac.jp', '1')
        DstControlParam = 1
    except socket.error, e:
        current_group3 = ET.SubElement(body2,'InetnetConnection')
        importValues(current_group3, 'wdc.kugi.kyoto-u.ac.jp', '1')
        DstControlParam = 1
    else:
        current_group3 = ET.SubElement(body2,'FileDownload')
        val1 =  strftime("%Y", gmtime())
        val2 =  strftime("%m", gmtime())
        val4 =  str(int(val1) - 2000)+val2
        filename1 = val1+val2+'DstPresenet.txt'
        filename2 = val1+val2+'DstLast.txt'
        yearmonth = val1+val2+'/dst'
        filen = '.for.request'

        if int(val2)-1 == 0:
            val3 = '12'
        elif int(val2)-1 > 9:
            val3 = str(int(val2)-1)
        else:
            val3 = "0"+str(int(val2)-1)

        if int(val3) == 12:
            val6 =  str(int(val1) - 1)
            val5 =  str(int(val1) - 2001)+val3
        else:
            val6 =  str(int(val1))
            val5 =  str(int(val1) - 2000)+val3
        yearmonth2 = val6+val3+'/dst'

        urlpath2 = 'http://wdc.kugi.kyoto-u.ac.jp/dst_realtime/'+yearmonth2+val5+filen
        try:
            remotefile1 = urllib2.urlopen(urlpath2,None,timeout = timeout_v)
            buff = remotefile1.read()
        except urllib2.URLError, e:
            importValues(current_group3, 'F'+filename2, '1')
            DstControlParam = 1
        except socket.timeout, e:
            importValues(current_group3, 'F'+filename2, '1')
            DstControlParam = 1
        except socket.error, e:
            importValues(current_group3, 'F'+filename2, '1')
            DstControlParam = 1
        else:
            FileDownload(buff, pathdown+filename2)
            importValues(current_group3, 'F'+filename2, '0')
            DstControlParam = 0
            DstData = DstFileRead(DstData, pathdown+filename2)
            
        urlpath = 'http://wdc.kugi.kyoto-u.ac.jp/dst_realtime/'+yearmonth+val4+filen
        try:
            remotefile = urllib2.urlopen(urlpath,None,timeout = timeout_v)
            buff1 = remotefile.read()
        except urllib2.URLError, e:
            importValues(current_group3, 'F'+filename1, '1')
            DstControlParam = 1
        except socket.timeout, e:
            importValues(current_group3, 'F'+filename1, '1')
            DstControlParam = 1
        except socket.error, e:
            importValues(current_group3, 'F'+filename1, '1')
            DstControlParam = 1
        else:
            FileDownload(buff1, pathdown+filename1)
            importValues(current_group3, 'F'+filename1, '0')
            DstControlParam = 0
            DstData = DstFileRead(DstData, pathdown+filename1)
            
    return (DstData,DstControlParam)
#Dst file read to list
def DstFileRead(DstData, filename):
    numbers = []
    l = []
    mounth =[]
    with open(filename) as f:
        for line in f.readlines():
            l = line.splitlines()
            numbers.append(l[0][20:116])
            mounth.append(l[0][5:7])
    for k in range(0,len(numbers)-2):
        i = 0
        j = 4
        while i < len(numbers[k]):
            DstData.append([int(mounth[0]),k+1,(j/4)-1,int(numbers[k][i:j])])
            i = i + 4
            j = j + 4
    return DstData
#Kp file download
def KpFileDownload(KpData,pathdown):
    timeout_v = 10
    socket.setdefaulttimeout(timeout_v)
    try:
        urlpath =urllib2.urlopen('http://www-app3.gfz-potsdam.de/kp_index/',None,timeout = timeout_v)
    except urllib2.URLError, e:
        current_group3 = ET.SubElement(body2,'InetnetConnection')
        importValues(current_group3, 'www-app3.gfz-potsdam.de', '1')
        KpControlParam = 1
    except socket.timeout, e:
        current_group3 = ET.SubElement(body2,'InetnetConnection')
        importValues(current_group3, 'www-app3.gfz-potsdam.de', '1')
        KpControlParam = 1
    except socket.error, e:
        current_group3 = ET.SubElement(body2,'InetnetConnection')
        importValues(current_group3, 'www-app3.gfz-potsdam.de', '1')
        KpControlParam = 1
    else:
        current_group3 = ET.SubElement(body2,'FileDownload')
        val1 =  strftime("%Y", gmtime())
        val2 =  strftime("%m", gmtime())
        filename1 = val1+val2+'KpPresenet.txt'
        filename2 = val1+val2+'KptLast.txt'
        urlpath2 = 'http://www-app3.gfz-potsdam.de/kp_index/pqlyymm.wdc'
        try:
            remotefile1 = urllib2.urlopen(urlpath2,None,timeout = timeout_v)
            buff = remotefile1.read()
        except urllib2.URLError, e:
            importValues(current_group3, 'F'+filename2, '1')
            KpControlParam = 1
        except socket.timeout, e:
            importValues(current_group3, 'F'+filename2, '1')
            KpControlParam = 1
        except socket.error, e:
            importValues(current_group3, 'F'+filename2, '1')
            KpControlParam = 1
        else:
            FileDownload(buff, pathdown+filename2)
            importValues(current_group3, 'F'+filename2, '0')
            KpControlParam = 0
            KpData = KpFileRead(KpData, pathdown+filename2)

        urlpath = 'http://www-app3.gfz-potsdam.de/kp_index/qlyymm.wdc'        
        try:
            remotefile = urllib2.urlopen(urlpath,None,timeout = timeout_v)
            buff1 = remotefile.read()
        except urllib2.URLError, e:
            importValues(current_group3, 'F'+filename1, '1')
            KpControlParam = 1
        except socket.timeout, e:
            importValues(current_group3, 'F'+filename1, '1')
            KpControlParam = 1
        except socket.error, e:
            importValues(current_group3, 'F'+filename1, '1')
            KpControlParam = 1
        else:
            FileDownload(buff1,pathdown+filename1)
            importValues(current_group3, 'F'+filename1, '0')
            KpControlParam = 0
            KpData = KpFileRead(KpData, pathdown+filename1)
    return (KpData,KpControlParam)
#Kp file read to list
def KpFileRead(KpData, filename):
    numbersKp = []
    numbersAp = []
    l = []
    mounth =[]
    with open(filename) as f:
        for line in f.readlines():
            l = line.splitlines()
            numbersKp.append(l[0][12:28])
            numbersAp.append(l[0][31:58])
            mounth.append(l[0][2:4])
    for k in range(0,len(numbersKp)):
        i = 0
        j = 2
        iap = 0
        jap = 3
        x = range(1,25)
        r = 3
        while i < len(numbersKp[k]):
            p = r - 3
            while p < r:
                if str(numbersAp[k][iap:jap]) != '   ':
                    ap = int(numbersAp[k][iap:jap])
                else:
                    ap = 999
                KpData.append([int(mounth[0]),k+1,x[p]-1,int(numbersKp[k][i:j]),ap])
                p = p + 1
            i = i + 2
            j = j + 2
            iap = iap + 3
            jap = jap + 3
            r = r + 3
    return KpData
#Ace Location File Download
def AceLocalFileDownload(pathdown):
    local = []
    val1 =  strftime("%Y", gmtime())
    val2 =  strftime("%m", gmtime())
    filename = val1+val2+'_ace_loc_1h.txt'
    letter = 'File'
    if int(val2)-1 == 0:
        val3 = '12'
    elif int(val2)-1 > 9:
        val3 = str(int(val2)-1)
    else:
        val3 = "0"+str(int(val2)-1)
        
    if int(val3) == 12:
        val6 =  str(int(val1) - 1)
    else:
        val6 =  str(int(val1))
    filename2 = val6+val3+'_ace_loc_1h.txt'

    current_group3 = ET.SubElement(body2,letter+'_ace_loc_1h.txt')
    try:
        os.stat(pathdown+filename2)
    except:
        importValues(current_group3, letter+filename2, '1')
    else:
        importValues(current_group3, letter+filename2, '0')
        local = AceDataFromFile(local, pathdown+filename2, 16)
    try:
        os.stat(pathdown+filename)
    except:
        importValues(current_group3, letter+filename, '1')
    else:
        importValues(current_group3, letter+filename, '0')
        local = AceDataFromFile(local, pathdown+filename, 16)
    return local
#----------------------------------------------------------------------------------------------------------------------------------
#Preprocessing data function (missing data, time shifting, hour data form, etc)
#Ace Missing data filling
def AceDataMissingDataFill(Mag, Swepam, local):
    AceData = []
    body3 = ET.SubElement(root, 'AceFileFormat')
    current_group4 = ET.SubElement(body3, 'FileFormat')
    if len(Mag)!=len(Swepam):
        importValues(current_group4, 'AceFileMagAndSwepam', '1')
    else:
        importValues(current_group4, 'AceFileMagAndSwepam', '0')
        for i in range(0,len(Mag)):
            if Mag[i][7] < -999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Mag[j][7])
                    Mag[i][7] = round(mean(x),1)
                else:
                    Mag[i][7] = 0.04 #mean(0.4) max(16.6)
            if Mag[i][8] < -999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Mag[j][8])
                    Mag[i][8] = round(mean(x),1)
                else:
                    Mag[i][8] = -0.11 #mean(-0.1065) max(33.9)
            if Mag[i][9] < -999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Mag[j][9])
                    Mag[i][9] = round(mean(x),1)
                else:
                    Mag[i][9] = -0.03 #mean(-0.03) max(39.5)
            if Mag[i][10] < -999:
                Mag[i][10] = round(sqrt(Mag[i][7]**2+Mag[i][8]**2+Mag[i][9]**2),2)
            if Mag[i][11] < -999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Mag[j][11])
                    Mag[i][11] = round(mean(x),1)
                else:
                    Mag[i][11] = -0.58 #mean(-0.5821) max(90)
            if Mag[i][12] < -999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Mag[j][12])
                    Mag[i][12] = round(mean(x),1)
                else:
                    Mag[i][12] = 180 #mean(180) max(360)
            if Swepam[i][7] < -999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Swepam[j][7])
                    Swepam[i][7] = round(mean(x),1)
                else:
                    Swepam[i][7] = 5.72 #mean(5.7246) max(100.9)
            if Swepam[i][8] < -9999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Swepam[j][8])
                    Swepam[i][8] = round(mean(x),1)
                else:
                    Swepam[i][8] = 440.8 #mean(440.8) max(1189)
            if Swepam[i][9] < -99999:
                if i > 15:
                    x = []
                    for j in range(i-10,i):
                        x.append(Swepam[j][9])
                    Swepam[i][9] = round(mean(x),1)
                else:
                    Swepam[i][9] = 10100 #mean(10100) max(663327)
            DeltaT = TimeShirt(Swepam[i],local)
            for x in Swepam[i][7:len(Swepam[i])]:
                Mag[i].insert(len(Mag[i]),x)
            Mag[i].insert(len(Mag[i]),DeltaT)
            AceData.append(Mag[i])
    return AceData
#Timeshifting
def TimeShirt(Swepam,local):
    DeltaT = -1
    local1 = 239
    local2 = 0
    for i in range(0,len(local)):
        if local[i][1] == Swepam[1] and local[i][2] == Swepam[2] and local[i][3] == 100*(int(Swepam[3]/100)):
            if local[i][6] == -999.9:
                W = tan(0.5*atan(Swepam[8]/428))
                DeltaT = (int(((6371*local1)/Swepam[8])*((1+((6371*local2)*W)/(6371*local1))/(1-(30*W)/Swepam[8]))/60))
            else:
                W = tan(0.5*atan(Swepam[8]/428))
                DeltaT = (int(((6371*local[i][6])/Swepam[8])*((1+((6371*local[i][7])*W)/(6371*local[i][6]))/(1-(30*W)/Swepam[8]))/60))
    if DeltaT == -1:
        W = tan(0.5*atan(Swepam[8]/428))
        DeltaT = (int(((6371*local[len(local)-1][6])/Swepam[8])*((1+((6371*local[len(local)-1][7])*W)/(6371*local[len(local)-1][6]))/(1-(30*W)/Swepam[8]))/60))
    return DeltaT
#Hour Data form and standart deviation calculation
def AceDataHourForm(AceDataMin):
    AceDataHour = []
    Temp = []
    k = 0
    j = 0
    for i in range(0,9):
        Temp.append([[i*0],[i*0],[i*0]])    
    while k < len(AceDataMin):
        for i in range(0,60):
            tempYY = int(AceDataMin[k][0])
            tempMM = int(AceDataMin[k][1])
            tempDD = int(AceDataMin[k][2])
            tempJD = int(AceDataMin[k][4])
            if int((int(AceDataMin[i+k][3]/100)*60+(AceDataMin[i+k][3]-(int(AceDataMin[i+k][3]/100)*100))+AceDataMin[i+k][16])/60) == j:
                for jj in range(0,len(Temp)):
                    Temp[jj][0].insert(len(Temp[jj][0]),AceDataMin[i+k][7+jj])
            elif int((int(AceDataMin[i+k][3]/100)*60+(AceDataMin[i+k][3]-(int(AceDataMin[i+k][3]/100)*100))+AceDataMin[i+k][16])/60) == j+1:
                for jj in range(0,len(Temp)):
                    Temp[jj][1].insert(len(Temp[jj][1]),AceDataMin[i+k][7+jj])
            elif int((int(AceDataMin[i+k][3]/100)*60+(AceDataMin[i+k][3]-(int(AceDataMin[i+k][3]/100)*100))+AceDataMin[i+k][16])/60) == j+2:
                for jj in range(0,len(Temp)):
                    Temp[jj][2].insert(len(Temp[jj][2]),AceDataMin[i+k][7+jj])

        if len(Temp[0][0]) == 1:
            for ii in range(0,len(Temp)):
                for jj in range(0,2):
                    Temp[ii][jj] = Temp[ii][jj+1]
                Temp[ii][2] = [0]
            if j < 23:
                j = j + 1
                k = k +60
            else:
                j = 0
                k = k +60
        elif len(Temp[0][0]) > 1:
            Data = [tempYY,tempMM,tempDD,j,tempJD]
            for ii in range(0,len(Temp)):
                Data.insert(len(Data),round(float(sum(Temp[ii][0]))/float(len(Temp[ii][0])-1),2))
            for ii in range(0,len(Temp)):
                Data.insert(len(Data),round(stdf(Temp[ii][0][1:]),2))
            AceDataHour.append(Data)
            for ii in range(0,len(Temp)):
                for jj in range(0,2):
                    Temp[ii][jj] = Temp[ii][jj+1]
                Temp[ii][2] = [0]
            if j < 23:
                j = j + 1
                k = k +60
            else:
                j = 0
                k = k +60
        else:
            k = k + 60
    if len(Temp[0][0]) > 1:
        if j - 1 < 0:
            jj = 23
        else:
            jj = j - 1
        now_date = datetime.datetime.utcnow().replace(tempYY,tempMM,tempDD,jj)
        delta = datetime.timedelta(hours=1)
        now_date = now_date + delta
        Data = [int(now_date.strftime("%Y")), int(now_date.strftime("%m")), int(now_date.strftime("%d")), int(now_date.strftime("%H")),tempJD]
        
        for ii in range(0,len(Temp)):
            Data.insert(len(Data),round(float(sum(Temp[ii][0]))/float(len(Temp[ii][0])-1),2))
        for ii in range(0,len(Temp)):
            Data.insert(len(Data),round(stdf(Temp[ii][0][1:]),2))
        AceDataHour.append(Data)

    if len(Temp[0][1]) > 1:
        if j - 1 < 0:
            jj = 23
        else:
            jj = j - 1
        now_date = datetime.datetime.utcnow().replace(tempYY,tempMM,tempDD,jj)
        delta = datetime.timedelta(hours=2)
        now_date = now_date + delta
        Data = [int(now_date.strftime("%Y")), int(now_date.strftime("%m")), int(now_date.strftime("%d")), int(now_date.strftime("%H")),tempJD]
        for ii in range(0,len(Temp)):
            Data.insert(len(Data),round(float(sum(Temp[ii][1]))/float(len(Temp[ii][1])-1),2))
        for ii in range(0,len(Temp)):
            Data.insert(len(Data),round(stdf(Temp[ii][1][1:]),2))
        AceDataHour.append(Data)
    return AceDataHour
#Forming output hour delay data with indeces
def AllDataCompound(AceDataH, DstData, KpData, DstControlParam, KpControlParam):
    if DstControlParam == 1 and KpControlParam != 1:
        for i in range(0,len(AceDataH)):
            k1 = 0
            for k in range(0,len(KpData)):
                if AceDataH[i][1] == KpData[k][0] and AceDataH[i][2] == KpData[k][1] and AceDataH[i][3] == KpData[k][2]:
                    k1 = 1
                    AceDataH[i].insert(len(AceDataH[i]),9999)
                    AceDataH[i].insert(len(AceDataH[i]),KpData[k][3])
                    AceDataH[i].insert(len(AceDataH[i]),KpData[k][4])
            if k1 != 1 and i < len(AceDataH):
                AceDataH[i].insert(len(AceDataH[i]),9999)
                AceDataH[i].insert(len(AceDataH[i]),99)
                AceDataH[i].insert(len(AceDataH[i]),999)
    elif KpControlParam == 1 and DstControlParam != 1:
        for i in range(0,len(AceDataH)):
            j1 = 0
            for j in range(0,len(DstData)):
                if AceDataH[i][1] == DstData[j][0] and AceDataH[i][2] == DstData[j][1] and AceDataH[i][3] == DstData[j][2]:
                    j1 = 1
                    AceDataH[i].insert(len(AceDataH[i]),DstData[j][3])
                    AceDataH[i].insert(len(AceDataH[i]),99)
                    AceDataH[i].insert(len(AceDataH[i]),999)
            if j1 != 1 and i < len(AceDataH):
                AceDataH[i].insert(len(AceDataH[i]),9999)
                AceDataH[i].insert(len(AceDataH[i]),99)
                AceDataH[i].insert(len(AceDataH[i]),999)
    elif DstControlParam == 1 and KpControlParam == 1:
        for i in range(0,len(AceDataH)):
            AceDataH[i].insert(len(AceDataH[i]),9999)
            AceDataH[i].insert(len(AceDataH[i]),99)
            AceDataH[i].insert(len(AceDataH[i]),999)
    else:
        for i in range(0,len(AceDataH)):
            k1 = 0
            j1 = 0
            for j in range(0,len(DstData)):
                if AceDataH[i][1] == DstData[j][0] and AceDataH[i][2] == DstData[j][1] and AceDataH[i][3] == DstData[j][2]:
                    j1 = 1
                    AceDataH[i].insert(len(AceDataH[i]),DstData[j][3])
            for k in range(0,len(KpData)):
                if AceDataH[i][1] == KpData[k][0] and AceDataH[i][2] == KpData[k][1] and AceDataH[i][3] == KpData[k][2]:
                    k1 = 1
                    AceDataH[i].insert(len(AceDataH[i]),KpData[k][3])
                    AceDataH[i].insert(len(AceDataH[i]),KpData[k][4])
            if j1 != 1 and i < len(AceDataH):
                AceDataH[i].insert(len(AceDataH[i]),9999)
            if k1 != 1 and i < len(AceDataH):
                AceDataH[i].insert(len(AceDataH[i]),99)
                AceDataH[i].insert(len(AceDataH[i]),999)
    return AceDataH
#----------------------------------------------------------------------------------------------------------------------------------
#Fractal calculation
#Linear regression parameters calculation 
class SimpleLinearRegression:
    def __init__(self, data):
        self.data = data   # list of (x,y) pairs
        self.a    = 0      # "a" of y = a + b*x
        self.b    = 0      # "b" of y = a + b*x
        self.r    = 0      # coefficient of correlation
    def run(self):
        sumX, sumY, sumXY, sumXX, sumYY = 0, 0, 0, 0, 0
        n = float(len(self.data))
        for x, y in self.data:
            sumX  += x
            sumY  += y
            sumXY += x*y
            sumXX += x*x
            sumYY += y*y
        denominator = sqrt((sumXX - 1/n * sumX**2)*(sumYY - 1/n * sumY**2))
        if denominator < 0.001:
            return False
        self.r  = (sumXY - 1/n * sumX * sumY)
        self.r /= denominator
        if abs(self.r) < 0.001:
            return False
        self.b  = sumXY - sumX * sumY / n
        self.b /= (sumXX - sumX**2 / n)
        self.a  = sumY - self.b * sumX
        self.a /= n
        return True
    def function(self, x):
        return self.a + self.b * x
    def __repr__(self):
        return "y = f(x) = %(a)f + %(b)f*x" % self.__dict__
#Main funtion of Ace data fractal dimention calculation (Hurst exponent and Minkowsky dimension)
def FractalCalcul(AceData, outputfile):
    RowsData = [[float(row[i]) for row in AceData] for i in range(len(AceData[0]))]
    Fractal = []
    k = 38880
    while k < len(RowsData[1])-360:
        D = []
        if k+360 < len(RowsData[1]):
            j = k+360
        else:
            j = len(RowsData[1])
        for i in range(0,len(RowsData)):
            if i < 3:
                D.insert(len(D),int(RowsData[i][j]))
            elif i == 3:
                D.insert(len(D),int((RowsData[i][j]))/100)
            elif i > 6 and i < 16:
                #D.insert(len(D),round(hurst(RowsData[i][k:j]),2))
                D.insert(len(D),round(2-hurst(RowsData[i][k:j]),2))
            elif i == 4:
                D.insert(len(D),int(RowsData[i][j]))
        Fractal.append(D)
        k = k + 60
    DataToFile(Fractal, outputfile)
#Standart Deviation calculation
def stdf(a):
    m = float(sum(a))/float(len(a))
    return sqrt(float(sum((x-m) ** 2 for x in a)) / float(len(a)))
#Hurst exponent calculation
def hurst(P):
    tau = []; lagvec = []
    tau2 = []; lagvec2 = []
    for lag in range(2,20):
        Dev = []
        W1 = P[:-lag]
        W2 = P[lag:]
        for i in range(0,len(W1)):
            Dev.append((W2[i]-W1[i]))
        lagvec.append(lag)
        tau.append(sqrt(stdf(Dev)))
    data = []
    for i in range(0,len(tau)):
        if round(lagvec[i],15) == 0:
            lagvec[i]= 0.00001
        if round(tau[i],15) == 0:
            tau[i]= 0.00001
        data.append([log10(lagvec[i]),log10(tau[i])])  
    linRegr = SimpleLinearRegression(data)
    if not linRegr.run():
        return 0
    return  round(2*abs(linRegr.b),2)
#model file read
def ModelFromFile(Model, filename, lines):
    count = 0
    with open(filename, 'r') as ff:
        for line in ff.readlines():
            if count > lines:
                Model.append (list (map (float, line.split())))
            else:
                count = count + 1
    return Model

#Prediction module
#main model prediction function
def prediction(Model, Data, lastpred, k):
    result = 0    
    if Data[k][9] == 99:
        Data[k][9] = lastpred
    if Data[k][10] == 9999:
        Data[k][10] = lastpred
    for x in Model:
        d = 1
        d*=x[1]
        j = 0
        if int(x[6]) > 1:
            for i in range(0,int(x[6])):
                d *=((Data[k-int(x[8+j])][int(x[7+j])-1])**(int(x[9+j])))
                j = j + 3
        elif int(x[6]) == 1 and int(x[7]) > 0:
            d *=((Data[k-int(x[8])][int(x[7])-1])**(int(x[9])))
        result += d
    return int(result)

#data form for prediction in model format
def DataForPredForm(Data):
    Data2 = []
    for j in range(0,len(Data)):
        DOY = int(datetime.datetime.utcnow().replace(int(Data[j][0]),int(Data[j][1]),int(Data[j][2]),int(Data[j][3])).strftime("%j"))
        Data2.append([Data[j][8],Data[j][9],Data[j][10],Data[j][5],Data[j][6],Data[j][7],Data[j][13],Data[j][11],Data[j][12],Data[j][24],Data[j][23],Data[j][25],round(sin((4*pi*(Data[j][3]-2))/24),4),round(cos((4*pi*(Data[j][3]-2))/4),4),round(sin((4*pi*(DOY-80))/365.24),4),round(cos((4*pi*(DOY-80))/365.24),4)])
    return Data2

#Output (results) matrix forming
def OutPredictionTimeData(Data, lag, tau, corrector, period):
    Outres = []
    for i in range(len(Data)-(lag+(period+1)+corrector),len(Data)+corrector,tau):
        if i+lag < len(Data):
            if tau == 1:
                Outres.append([int(Data[i+lag][0]),int(Data[i+lag][1]),int(Data[i+lag][2]),int(Data[i+lag][3]),int(Data[i+lag][23]),9999,9999,9999,9999])
            else:
                Outres.append([int(Data[i+lag][0]),int(Data[i+lag][1]),int(Data[i+lag][2]),int(Data[i+lag][3]),int(Data[i+lag][24]),99])
        else:
            now_date = datetime.datetime.utcnow().replace(int(Data[i][0]),int(Data[i][1]),int(Data[i][2]),int(Data[i][3]))
            delta = datetime.timedelta(hours=lag)
            now_date1 = now_date + delta
            if tau == 1:
                Outres.append([int(now_date1.strftime("%Y")), int(now_date1.strftime("%m")), int(now_date1.strftime("%d")), int(now_date1.strftime("%H")), 9999, 9999, 9999, 9999, 9999])
            else:
                Outres.append([int(now_date1.strftime("%Y")), int(now_date1.strftime("%m")), int(now_date1.strftime("%d")), int(now_date1.strftime("%H")), 99, 99])
    return Outres

#function of iterative 24 hours history prediction calling prediction function 
def PredictionDrive(Model, Data2, lag, tau, corrector, Index, period):
    Pred = []
    Pres = []
    for i in range(len(Data2)-(lag+(period+1)+corrector),len(Data2)+corrector,tau):
        if len(Pred) > 0:
            Pred.append(prediction(Model, Data2, Pred[len(Pred)-1], i))
        else:
            Pred.append(prediction(Model, Data2, 0, i))
        if Index == 1:
            Pres.append(Data2[i][10])
        elif Index == 2:
            Pres.append(Data2[i][9])
    return (Pred,Pres)

#function for accumulating results of prediction achived with different models 
def PredictionProcessor(ModelFile, Data, model, outputfile):
    if (len(Data)- 54) < 642:
        period = 24
    else:
        period = 48
    if model == 1:
        Pred = [0,0,0,0]
        Pres = [0,0,0,0]
        Outres = OutPredictionTimeData(Data, 4, 1, 0, period)
        for j in range(0,4):
            Model = []
            ModelFromFile(Model, ModelFile[j], -1)
            (Pred[j],Pres[j]) = PredictionDrive(Model, DataForPredForm(Data), j+1, 1, 0, 1, period)
        for i in range(0,len(Pred)):
            for k in range(0,len(Pred[i])):
                Outres[k][i+5] = Pred[i][k]
        Pres = [[int(row[i]) for row in Pres] for i in range(len(Pres[0]))]
        realdata = []
        presistent = []
        prediction = []
        for i in range(0,len(Outres)):
                if Outres[i][4] != 9999:
                    realdata.append(Outres[i][4])
                    presistent.append(Pres[i])
                    prediction.append(Outres[i][5:])
        MetaXMLfileGeneration(prediction, presistent, realdata, outputfile, 'Dst')
    elif model == 2:
        Pred = [0]
        Pres = [0]
        Model = []
        now_date = datetime.datetime.utcnow().replace(int(Data[len(Data)-1][0]),int(Data[len(Data)-1][1]),int(Data[len(Data)-1][2]),int(Data[len(Data)-1][3]))
        t = int(now_date.strftime("%H"))
        if t == 0 or t == 3 or t == 6 or t == 9 or t == 12 or t == 15 or t==18 or t == 21:
            corrector = 0
        else:
            corrector = 1
        Outres = OutPredictionTimeData(Data, 3, 3, corrector,period)
        ModelFromFile(Model, ModelFile, -1)
        (Pred, Pres) = PredictionDrive(Model, DataForPredForm(Data), 3, 3, corrector, 2, period)
        for j in range(0,len(Pred)):
            Outres[j][5] = Pred[j]
        realdata = []
        presistent = []
        prediction = []
        for i in range(0,len(Outres)):
                if Outres[i][4] != 99:
                    realdata.append(Outres[i][4])
                    presistent.append(Pres[i])
                    prediction.append(Outres[i][5])
        MetaXMLfileGeneration(prediction, presistent, realdata, outputfile, 'Kp')
    return Outres
#function for starting prediction of Dst and Kp and writing results
def PredictionModule(Data, outputfile, dirs, DstControlParam, KpControlParam):
    test = int(datetime.datetime.utcnow().strftime("%H"))
    fileout = datetime.datetime.utcnow().strftime("%Y")+datetime.datetime.utcnow().strftime("%m")+datetime.datetime.utcnow().strftime("%d")+datetime.datetime.utcnow().strftime("%H")+datetime.datetime.utcnow().strftime("%M")+'_'+outputfile
    ModelsDst = ['DST_1101.RES', 'DST_1102.RES','DST_1103.RES', 'DST_1104.RES']
    
    if KpControlParam == 0:
        if test == 0 or test == 3 or test == 6 or test == 9 or test == 12 or test == 15 or test == 18 or test == 21:
            DataToFile(PredictionProcessor('KP_1003.RES', Data, 2, dirs+"Kp_"+fileout+".xml"), dirs+"Kp_"+fileout+".asc")
            #copy the latest forecast to a file with a fixed name - AP
            shutil.copyfile(dirs+"Kp_"+fileout+".asc", dirs+"Kp_latest.asc")
            shutil.copyfile(dirs+"Kp_"+fileout+".xml", dirs+"Kp_latest.xml")

    if DstControlParam == 0:
        DataToFile(PredictionProcessor(ModelsDst, Data, 1, dirs+"Dst_"+fileout+".xml"), dirs+"Dst_"+fileout+".asc")
        #copy the latest forecast to a file with a fixed name - AP
        shutil.copyfile(dirs+"Dst_"+fileout+".asc", dirs+"Dst_latest.asc")
        shutil.copyfile(dirs+"Dst_"+fileout+".xml", dirs+"Dst_latest.xml")
#function error statistic parameters test
def MetaXMLfileGeneration(Predictions, Presistent, realdata, outputfile, Index):
    if Index == 'Dst':
        TestFilebody = ET.SubElement(TestFileroot, 'Predictand')
        ET.SubElement(TestFilebody, 'Name').text = Index
        ET.SubElement(TestFilebody, 'Units').text = 'nT'
        Testsubbody = ET.SubElement(TestFilebody, 'List_of_models')
        for j in range(0,4):
            Testsubbody2 = ET.SubElement(Testsubbody, 'Model')
            ET.SubElement(Testsubbody2, 'Lead_Time').text = str(j+1)
            presist = [[int(row[i]) for row in Presistent] for i in range(len(Presistent[0]))]
            predict = [[int(row[i]) for row in Predictions] for i in range(len(Predictions[0]))]
            MetaDataToXml(Testsubbody2, predict[j], presist[j], realdata)
            XmlToFileWr(TestFileroot, outputfile)
    else:
            TestFileroot2 = ET.Element('MetaFile')
            ET.SubElement(TestFileroot2, 'productname').text = 'Geomagnetic forecast module'
            ET.SubElement(TestFileroot2, 'stationname').text = 'diverse global'
            ET.SubElement(TestFileroot2, 'project').text = 'AFFECTS'
            ET.SubElement(TestFileroot2, 'product_creation_time').text = generated_on
            ET.SubElement(TestFileroot2, 'location').text = 'global'
            ET.SubElement(TestFileroot2, 'format').text = 'ASCII'
            ET.SubElement(TestFileroot2, 'data_type').text = 'nc'
            ET.SubElement(TestFileroot2, 'description').text = 'This forecast is issued without any warranties - use at your own risk'
            ET.SubElement(TestFileroot2, 'contact_person').text = 'Aleksei Parnowski parnowski@ikd.kiev.ua'
            ET.SubElement(TestFileroot2, 'contact_address').text = 'Space Research Institute, Prospekt Akademika Glushkova 40 building 4/1, 03680 MSP Kyiv-187, Ukraine'
            ET.SubElement(TestFileroot2, 'Processing_software_version').text = '2.0'    
            TestFile = ET.SubElement(TestFileroot2, 'Predictand')
            ET.SubElement(TestFile, 'Name').text = Index
            ET.SubElement(TestFile, 'Units').text = 'nT'
            Testsub = ET.SubElement(TestFile, 'List_of_models')
            Testsub2 = ET.SubElement(Testsub, 'Model')
            ET.SubElement(Testsub2, 'Lead_Time').text = str(3)
            TestFile2 = ET.SubElement(Testsub2, 'Performance_Score')
            ET.SubElement(TestFile2, 'Name').text = 'Mean square error'
            ET.SubElement(TestFile2, 'Value').text = str(round(MSE_func(Predictions,realdata),3))
            ET.SubElement(TestFile2, 'Persistence_Value').text = str(round(MSE_func(Presistent,realdata),3))
            TestFile3 = ET.SubElement(Testsub2, 'Performance_Score2')
            ET.SubElement(TestFile3, 'Name').text = 'Pearsons linear correlation coefficient'
            ET.SubElement(TestFile3, 'Value').text = str(round(pearson_corr(Predictions,realdata),3))
            ET.SubElement(TestFile3, 'Persistence_Value').text = str(round(pearson_corr(Presistent,realdata),3))
            TestFile4 = ET.SubElement(Testsub2, 'Performance_Score')
            Metafilecurrent_group = ET.SubElement(TestFile4, 'Name').text = 'Prediction efficiency'
            Metafilecurrent_group = ET.SubElement(TestFile4, 'Value').text = str(round(PredEf(Predictions,realdata),3))
            Metafilecurrent_group = ET.SubElement(TestFile4, 'Persistence_Value').text = str(round(PredEf(Presistent,realdata),3))
            TestFile5 = ET.SubElement(Testsub2, 'Performance_Score')
            Metafilecurrent_group = ET.SubElement(TestFile5, 'Name').text = 'Skill score'
            Metafilecurrent_group = ET.SubElement(TestFile5, 'Value').text = str(round(1-(MSE_func(Predictions,realdata))/(MSE_func(Presistent,realdata)),3))
            XmlToFileWr(TestFileroot2, outputfile)
#function error statistic parameters sets in xml
def MetaDataToXml(TestFilebody, Predictions, Presistent, realdata):
    TestFilebody2 = ET.SubElement(TestFilebody, 'Performance_Score')
    ET.SubElement(TestFilebody2, 'Name').text = 'Mean square error'
    ET.SubElement(TestFilebody2, 'Value').text = str(round(MSE_func(Predictions,realdata),3))
    ET.SubElement(TestFilebody2, 'Persistence_Value').text = str(round(MSE_func(Presistent,realdata),3))
    TestFilebody3 = ET.SubElement(TestFilebody, 'Performance_Score2')
    ET.SubElement(TestFilebody3, 'Name').text = 'Pearsons linear correlation coefficient'
    ET.SubElement(TestFilebody3, 'Value').text = str(round(pearson_corr(Predictions,realdata),3))
    ET.SubElement(TestFilebody3, 'Persistence_Value').text = str(round(pearson_corr(Presistent,realdata),3))
    TestFilebody4 = ET.SubElement(TestFilebody, 'Performance_Score')
    Metafilecurrent_group = ET.SubElement(TestFilebody4, 'Name').text = 'Prediction efficiency'
    Metafilecurrent_group = ET.SubElement(TestFilebody4, 'Value').text = str(round(PredEf(Predictions,realdata),3))
    Metafilecurrent_group = ET.SubElement(TestFilebody4, 'Persistence_Value').text = str(round(PredEf(Presistent,realdata),3))
    TestFilebody5 = ET.SubElement(TestFilebody, 'Performance_Score')
    Metafilecurrent_group = ET.SubElement(TestFilebody5, 'Name').text = 'Skill score'
    Metafilecurrent_group = ET.SubElement(TestFilebody5, 'Value').text = str(round(1-(MSE_func(Predictions,realdata))/(MSE_func(Presistent,realdata)),3))
#Mean square error
def MSE_func(y,x):
    RSS = RSS_func(y,x)
    return sqrt(average(RSS))
#RSS
def RSS_func(y,x):
    RSS = []
    for i in range(0,len(x)):
        RSS.append((x[i]-y[i])**2)
    return RSS
#Pearcon Correlation coefficient
def pearson_corr(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff
    return diffprod / sqrt(xdiff2 * ydiff2)
#Prediction efficiency
def PredEf(y,x):
    MSE = MSE_func(y,x)
    RSS_x = []
    mean_x = mean(x)
    for i in range(0,len(x)):
        RSS_x.append((x[i]-mean_x)**2)
    MSE_x = sqrt(float(sum(RSS_x))/float(len(RSS_x)-1))
    return 1 - (MSE/MSE_x)**2
#additional functions
def mean(x):
    summ = 0.0
    for i in x:
         summ += i
    return float(summ) / float(len(x))
def average(x):
    assert len(x) > 0
    return float(sum(x)) / float(len(x))
#----------------------------------------------------------------------------------------------------------------------------------
#Main script of calling preprocessing and processing functions
def processorr(filename, dirs):
    AceDataMag = []
    AceDataSwepam = []
    DstData = []
    KpData = []
    body_prepro = ET.SubElement(root, 'DataProcessing')
    body_process = ET.SubElement(root, 'PredictionResults')
    current_group_prepro3 = ET.SubElement(body_prepro, 'MissingDataAndTimeShifting')
    current_group_prepro4 = ET.SubElement(body_prepro, 'HourDataForming')
    current_group_prepro5 = ET.SubElement(body_prepro, 'AllDataCompound')
    current_group_prepro6 = ET.SubElement(body_process, 'PredictionModule')
    name1 = '_ace_swepam_1m.txt'
    name2 = '_ace_mag_1m.txt'
    (AceDataMag,flag1) = AceDownload(name2, AceDataMag, 19, outpathcheck(dirs[0]))
    (AceDataSwepam,flag2) = AceDownload(name1, AceDataSwepam, 17, outpathcheck(dirs[1]))
    (DstData, DstControlParam) = DstFileDownload(DstData, outpathcheck(dirs[3]))
    (KpData, KpControlParam) = KpFileDownload(KpData, outpathcheck(dirs[3]))
    AceLocalPos = AceLocalFileDownload(outpathcheck(dirs[2]))
    fileout = datetime.datetime.utcnow().strftime("%Y")+datetime.datetime.utcnow().strftime("%m")+datetime.datetime.utcnow().strftime("%d")+datetime.datetime.utcnow().strftime("%H")
    if len(AceLocalPos) != 0:
        AceDataMin = AceDataMissingDataFill(AceDataMag, AceDataSwepam, AceLocalPos)
        del(AceDataMag)
        del(AceDataSwepam)
        if len(AceDataMin) != 0:
            importValues(current_group_prepro3, 'MinDataProcess', '0')
            DataToFile(AceDataMin, outpathcheck(dirs[4])+filename[0])
            AceDataHour = AceDataHourForm(AceDataMin)
            if (flag1 + flag2) != 0:
                FractalCalcul(AceDataMin, outpathcheck(dirs[4])+fileout+filename[2])
            del(AceDataMin)
            if len(AceDataHour) != 0:
                importValues(current_group_prepro4, 'HourDataProcess', '0')
                if len(DstData) + len(KpData)!=0:
                    AceOmni = AllDataCompound(AceDataHour, DstData, KpData, DstControlParam, KpControlParam)
                    importValues(current_group_prepro5, 'AllDataProcess', '0')
                    DataToFile(AceOmni, outpathcheck(dirs[4])+fileout+filename[1])
                    importValues(current_group_prepro6, 'PredictionModuleStart', '0')
                    PredictionModule(AceOmni, filename[3], outpathcheck(dirs[4]),DstControlParam, KpControlParam)
                    del(AceDataHour)
                    del(DstData)
                    del(KpData)
                    del(AceOmni)
                else:
                    importValues(current_group_prepro5, 'AllDataProcess', '1')
                    importValues(current_group_prepro6, 'PredictionModuleStart', '1')
            else:
                importValues(current_group_prepro4, 'HourDataProcess', '1')
        else:
            importValues(current_group_prepro3, 'MinDataProcess', '1')
            AceDataMag = []
            AceDataSwepam = []
            DstData = []
            KpData = []
            (AceDataMag,flag1) = AceDownload(name2, AceDataMag, 19, outpathcheck(dirs[0]))
            (AceDataSwepam,flag2) = AceDownload(name1, AceDataSwepam, 17, outpathcheck(dirs[1]))
            (DstData, DstControlParam) = DstFileDownload(DstData, outpathcheck(dirs[3]))
            (KpData, KpControlParam) = KpFileDownload(KpData, outpathcheck(dirs[3]))
            AceDataMin = AceDataMissingDataFill(AceDataMag, AceDataSwepam, AceLocalPos)
            del(AceDataMag)
            del(AceDataSwepam)
            if len(AceDataMin) != 0:
                importValues(current_group_prepro3, 'MinDataProcess_test2', '0')
                DataToFile(AceDataMin, outpathcheck(dirs[4])+filename[0])
                AceDataHour = AceDataHourForm(AceDataMin)
                FractalCalcul(AceDataMin, outpathcheck(dirs[4])+fileout+filename[2])
                del(AceDataMin)
                if len(AceDataHour) != 0:
                    importValues(current_group_prepro4, 'HourDataProcess', '0')
                    if len(DstData) + len(KpData)!=0:
                        AceOmni = AllDataCompound(AceDataHour, DstData, KpData, DstControlParam, KpControlParam)
                        importValues(current_group_prepro5, 'AllDataProcess', '0')
                        DataToFile(AceOmni, outpathcheck(dirs[4])+fileout+filename[1])
                        importValues(current_group_prepro6, 'PredictionModuleStart', '0')
                        PredictionModule(AceOmni, filename[3], outpathcheck(dirs[4]), DstControlParam, KpControlParam)
                        del(AceDataHour)
                        del(DstData)
                        del(KpData)
                        del(AceOmni)
                    else:
                        importValues(current_group_prepro5, 'AllDataProcess', '1')
                        importValues(current_group_prepro6, 'PredictionModuleStart', '1')
                else:
                    importValues(current_group_prepro4, 'HourDataProcess', '1')
            else:
                importValues(current_group_prepro3, 'MinDataProcess_test2', '1')
    else:
        AceLocalPos = AceLocalFileDownload(outpathcheck(dirs[2]))
        if len(AceLocalPos) != 0:
            AceDataMin = AceDataMissingDataFill(AceDataMag, AceDataSwepam, AceLocalPos)
            del(AceDataMag)
            del(AceDataSwepam)
            if len(AceDataMin) != 0:
                importValues(current_group_prepro3, 'MinDataProcess', '0')
                DataToFile(AceDataMin, outpathcheck(dirs[4])+filename[0])
                AceDataHour = AceDataHourForm(AceDataMin)
                if (flag1 + flag2) != 0:
                    FractalCalcul(AceDataMin, outpathcheck(dirs[4])+fileout+filename[2])
                del(AceDataMin)
                if len(AceDataHour) != 0:
                    importValues(current_group_prepro4, 'HourDataProcess', '0')
                    if len(DstData) + len(KpData)!=0:
                        AceOmni = AllDataCompound(AceDataHour, DstData, KpData, DstControlParam, KpControlParam)
                        importValues(current_group_prepro5, 'AllDataProcess', '0')
                        DataToFile(AceOmni, outpathcheck(dirs[4])+fileout+filename[1])
                        importValues(current_group_prepro6, 'PredictionModuleStart', '0')
                        PredictionModule(AceOmni, filename[3], outpathcheck(dirs[4]),DstControlParam, KpControlParam)
                        del(AceDataHour)
                        del(DstData)
                        del(KpData)
                        del(AceOmni)
                    else:
                        importValues(current_group_prepro5, 'AllDataProcess', '1')
                        importValues(current_group_prepro6, 'PredictionModuleStart', '1')
                else:
                    importValues(current_group_prepro4, 'HourDataProcess', '1')
            else:
                importValues(current_group_prepro3, 'MinDataProcess', '1')
                AceDataMag = []
                AceDataSwepam = []
                DstData = []
                KpData = []
                (AceDataMag,flag1) = AceDownload(name2, AceDataMag, 19, outpathcheck(dirs[0]))
                (AceDataSwepam,flag2) = AceDownload(name1, AceDataSwepam, 17, outpathcheck(dirs[1]))
                (DstData, DstControlParam) = DstFileDownload(DstData, outpathcheck(dirs[3]))
                (KpData, KpControlParam) = KpFileDownload(KpData, outpathcheck(dirs[3]))
                AceDataMin = AceDataMissingDataFill(AceDataMag, AceDataSwepam, AceLocalPos)
                del(AceDataMag)
                del(AceDataSwepam)
                if len(AceDataMin) != 0:
                    importValues(current_group_prepro3, 'MinDataProcess_test2', '0')
                    DataToFile(AceDataMin, outpathcheck(dirs[4])+filename[0])
                    AceDataHour = AceDataHourForm(AceDataMin)
                    FractalCalcul(AceDataMin, outpathcheck(dirs[4])+fileout+filename[2])
                    del(AceDataMin)
                    if len(AceDataHour) != 0:
                        importValues(current_group_prepro4, 'HourDataProcess', '0')
                        if len(DstData) + len(KpData)!=0:
                            AceOmni = AllDataCompound(AceDataHour, DstData, KpData, DstControlParam, KpControlParam)
                            importValues(current_group_prepro5, 'AllDataProcess', '0')
                            DataToFile(AceOmni, outpathcheck(dirs[4])+fileout+filename[1])
                            importValues(current_group_prepro6, 'PredictionModuleStart', '0')
                            PredictionModule(AceOmni, filename[3], outpathcheck(dirs[4]), DstControlParam, KpControlParam)
                            del(AceDataHour)
                            del(DstData)
                            del(KpData)
                            del(AceOmni)
                        else:
                            importValues(current_group_prepro5, 'AllDataProcess', '1')
                            importValues(current_group_prepro6, 'PredictionModuleStart', '1')
                    else:
                        importValues(current_group_prepro4, 'HourDataProcess', '1')
                else:
                    importValues(current_group_prepro3, 'MinDataProcess_test2', '1')
        else:
            importValues(current_group_prepro3, 'MinDataProcess', '1')
            importValues(current_group_prepro4, 'HourDataProcess', '1')
            importValues(current_group_prepro5, 'AllDataProcess', '1')
            importValues(current_group_prepro6, 'PredictionModuleStart', '1')
    outfilename = outpathcheck(dirs[4])+fileout+datetime.datetime.utcnow().strftime("%M")+filename[4]
    XmlToFileWr(root, outfilename)



